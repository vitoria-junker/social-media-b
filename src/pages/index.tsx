import Image from 'next/image'
import { Inter } from 'next/font/google'
import Link from 'next/link'
import Seo from '@/components/Seo'

const inter = Inter({ subsets: ['latin'] })

export default function Home() {
  return (
    <>
      <Seo />

      <main className={''}>
        <ul>
          <li>
            <Link href='/app'>APP</Link>
          </li>
          <li>
            <Link href='/devpleno'>Tenant Home</Link>
          </li>
        </ul>
      </main>
    </>
  )
}
